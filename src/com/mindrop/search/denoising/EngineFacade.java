package com.mindrop.search.denoising;

import java.io.IOException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author jcrcano
 */
public class EngineFacade {

    public static void main(String[] args) throws IOException {
         
        // Rules
        JSONArray rules = new JSONArray();

        //For each rule
        for (int i = 0; i < 4; i++) {
            JSONObject rule = new JSONObject();
            rule.put("type", "url " + i);
            rules.put(rule);
        }
        //end for

        JSONArray models = new JSONArray();
        //mp
        //for
        JSONObject mp = new JSONObject();
        mp.put("sim", new Float(1000.3423));
        mp.put("features", new Integer(34));
        models.put(mp);
        // end for

        JSONObject response = new JSONObject();

        response.put("noise", true);
        response.put("rules", rules);
        response.put("mp", models);
       
        System.out.println(response);        
    }
}
